package org.apnet.demo;

import org.apnet.demo.domain.AlumnoEntity;
import org.apnet.demo.repository.AlumnoRepository;
import org.apnet.demo.repository.CursoRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootAngularDemoMavenDomainApplication.class)
public class SpringBootAngularDemoMavenDomainApplicationTests {

	@Autowired
	private AlumnoRepository alumnoRepository;

	@Autowired
	private CursoRepository cursoRepository;

	@Test
	@Sql(statements ={"insert into Alumno_Entity (codigo,nombre) values ('0512002055','Germán Arellano Pozo')"})
	public void addAlumnoCurso() throws Exception {
		for(AlumnoEntity alumno : alumnoRepository.findAll()) {
			System.out.println(alumno);
		}
	}

	/*@Test
	public void findAlumnos(){
		for(AlumnoEntity alumno : alumnoRepository.findAll()) {
			System.out.println(alumno.getCodigo());
		}
	}*/
}
