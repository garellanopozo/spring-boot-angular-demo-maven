package org.apnet.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by German on 07/06/2016.
 */
@SpringBootApplication
public class SpringBootAngularDemoMavenDomainApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootAngularDemoMavenDomainApplication.class, args);
    }
}
