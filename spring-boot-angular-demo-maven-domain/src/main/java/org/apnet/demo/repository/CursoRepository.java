package org.apnet.demo.repository;

import org.apnet.demo.domain.CursoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by German on 02/06/2016.
 */
public interface CursoRepository extends JpaRepository<CursoEntity,Long>{
}
