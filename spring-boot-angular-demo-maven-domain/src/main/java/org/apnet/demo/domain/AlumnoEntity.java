package org.apnet.demo.domain;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by German on 02/06/2016.
 */
@Entity
@Data
public class AlumnoEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "codigo")
    private String codigo;
    @Column(name = "nombre")
    private String nombre;
}
