package org.apnet.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;


@SpringBootApplication
public class SpringBootAngularDemoMavenWebTomcatLocalApplication extends SpringBootServletInitializer {

	private static Class<SpringBootAngularDemoMavenWebTomcatLocalApplication> applicationClass = SpringBootAngularDemoMavenWebTomcatLocalApplication.class;

	public static void main(String[] args) {
		SpringApplication.run(applicationClass, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(applicationClass);
	}
}
