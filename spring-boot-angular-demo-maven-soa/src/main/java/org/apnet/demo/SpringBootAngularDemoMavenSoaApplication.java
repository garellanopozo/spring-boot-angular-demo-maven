package org.apnet.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAngularDemoMavenSoaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootAngularDemoMavenSoaApplication.class, args);
	}
}
