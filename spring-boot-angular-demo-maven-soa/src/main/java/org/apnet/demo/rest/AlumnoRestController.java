package org.apnet.demo.rest;

import org.apnet.demo.domain.AlumnoEntity;
import org.apnet.demo.repository.AlumnoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by German on 06/06/2016.
 */
@Controller
@RestController("/alumnno")
public class AlumnoRestController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    AlumnoRepository alumnoRepository;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public void addAlumno(@RequestBody AlumnoEntity alumno){
        this.alumnoRepository.save(alumno);
    }

    @RequestMapping(value = "/details", method = RequestMethod.GET)
    public List<AlumnoEntity> getAlumnoList(){
        return alumnoRepository.findAll();
    }
}
